import { NestFactory } from '@nestjs/core';
import { AppModule } from './appointments/appointments.module';
import { SERVER_PORT } from './config';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(SERVER_PORT);
}
bootstrap();
