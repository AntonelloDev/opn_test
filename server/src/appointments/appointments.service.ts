import { Injectable } from '@nestjs/common';
import { Appointment } from './appointments.entity';
import { Op } from 'sequelize';
import names from '../../static/names.json';

@Injectable()
export class AppointmentService {
  async getAppointments(): Promise<object[]> {
    try {
      const promises = names.map((name) =>
        Appointment.findAll({
          where: {
            name,
          },
        }),
      );
      const result = await Promise.all(promises);
      return result
        .filter((result) => result?.length > 0)
        .map((appointments) => ({
          name: appointments[0]?.name,
          appointments,
        }));
    } catch (error) {
      throw error;
    }
  }
  async createAppointments(payload): Promise<object> {
    try {
      const where = {
        name: payload.name,
        [Op.or]: [
          {
            [Op.and]: [
              { start: { [Op.lt]: payload.start } },
              { end: { [Op.gt]: payload.start } },
            ],
          },
          {
            [Op.and]: [
              { start: { [Op.lt]: payload.end } },
              { end: { [Op.gt]: payload.end } },
            ],
          },
          {
            [Op.and]: [
              { start: { [Op.gt]: payload.start } },
              { end: { [Op.lt]: payload.end } },
            ],
          },
        ],
      };
      const element = await Appointment.findAll({ where });
      if (element?.length > 0) {
        return {
          error: true,
        };
      }
      await Appointment.create(payload);
      return {
        ok: true,
      };
    } catch (error) {
      throw error;
    }
  }
}
