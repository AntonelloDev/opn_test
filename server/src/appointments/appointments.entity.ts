import { Table, Column, Model } from 'sequelize-typescript';

@Table
export class Appointment extends Model {
  @Column
  start: string;

  @Column
  end: string;

  @Column
  space: string;

  @Column
  name: string;
}
