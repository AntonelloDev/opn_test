import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request from 'supertest';
import { AppModule } from './appointments.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  }, 10000);

  test('/ (GET) Should return 200', () => {
    return request(app.getHttpServer()).get('/').expect(200);
  });
  test('/ (GET) Should return 404 for not existing routes', () => {
    return request(app.getHttpServer()).get('/not-existing-route').expect(404);
  });
  test('/ (POST) Should prevent users from adding appointment for not supported names', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        start: '20:00',
        end: '21:00',
        name: 'Emilio',
        space: 'kitchen',
      })
      .expect(409);
  });
  test('/ (POST) Should prevent users from adding appointment for not supported space', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        start: '18:00',
        end: '19:00',
        name: 'Bill',
        space: 'kitchenette',
      })
      .expect(409);
  });
});
