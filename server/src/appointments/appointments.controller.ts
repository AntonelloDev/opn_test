import {
  Controller,
  Get,
  Post,
  Res,
  Body,
  HttpStatus,
  Delete,
} from '@nestjs/common';
import { AppointmentService } from './appointments.service';
import CreateAppointmentDto from './create-appointment-dto';
import { Response } from 'express';
import names from '../../static/names.json';
import spaces from '../../static/spaces.json';
import { min, max } from '../../static/times.json';

@Controller()
export class AppController {
  constructor(private readonly appService: AppointmentService) {}

  @Get()
  async getAppointments(): Promise<object[]> {
    return await this.appService.getAppointments();
  }
  @Post()
  async createAppointments(
    @Body() payload: CreateAppointmentDto,
    @Res() res: Response,
  ): Promise<void> {
    let validationError = null;
    // START must be less than END (unless end is 00:00)
    if (payload.start >= payload.end) {
      validationError = 'START TIME MUST BE LESS THAN END';
    }
    // START must be between 00:30 and 24:00
    if (payload.start > max || payload.start < min) {
      validationError = 'START MUST BE BETWEEN 00:30 AND 24:00';
    }
    // END must be less than 23:30 OR must be 00:00.
    if (payload.end > max) {
      validationError = 'END MUST BE LESS OR EQUAL TO 00:00';
    }
    // Spaces must be in one of the 3 predefined spaces
    if (!spaces.includes(payload.space)) {
      validationError = 'SPACES MUST BE ONE OF :' + spaces;
    }
    // Names must be in one of the 3 predefined names
    if (!names.includes(payload.name)) {
      validationError = 'NAMES MUST BE ONE OF :' + names;
    }
    if (validationError) {
      res.status(HttpStatus.CONFLICT).json({ message: validationError });
    } else {
      interface resp {
        ok?: Boolean;
        error?: Boolean;
      }
      try {
        const response: resp = await this.appService.createAppointments(
          payload,
        );
        if (response.error) {
          res
            .status(HttpStatus.CONFLICT)
            .json({ message: 'DUPLICATE TIME SLOT' });
        } else res.status(HttpStatus.CREATED).json(response);
      } catch (error) {
        throw error;
      }
    }
  }
}
