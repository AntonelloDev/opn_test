import { Module } from '@nestjs/common';
import { AppController } from './appointments.controller';
import { AppointmentService } from './appointments.service';
import { databaseProviders } from '../database.providers';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppointmentService, ...databaseProviders],
})
export class AppModule {}
