export default class CreateAppointmentDto {
  start: string;
  end: string;
  space: string;
  name: string;
}
