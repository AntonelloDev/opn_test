export const DATABASE_NAME = '';
export const DATABASE_USER = '';
export const DATABASE_PASSWORD = '';
export const DATABASE_HOST = '';
export const DATABASE_PORT = 5432;
export const SERVER_PORT = 4000;
