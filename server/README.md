# opn-store-interview-challenge/house-timeline-full CLIENT

- Add the DB credentials etc ... in `/src/config.ts`
- `npm i` to install the dependencies <br/>
- `npm run start:dev` to run in dev mode <br/>
- `npm start` to run the production build. <br/>
- `npm run test` to run the test suite <br/>
- `You can view the docs with postman importing docs.json` <br/>
