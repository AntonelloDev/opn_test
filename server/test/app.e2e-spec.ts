import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/appointments/appointments.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/')
      .send({
        start: '20:00',
        end: '21:00',
        name: 'Emilio',
        space: 'kitchen',
      })
      .expect(409)
      .expect({ message: 'INVALID NAME' });
  });
});
