import * as types from './appointments.types';
/**
 *
 * @returns {object}
 */
export const getAppointmentsRequest = () => {
  return {
    type: types.GET_APPOINTMENTS_REQUEST,
  };
};
/**
 *
 * @param {object[]} data
 * @returns {object}
 */
export const getAppointmentsSuccess = (data) => {
  return {
    type: types.GET_APPOINTMENTS_SUCCESS,
    payload: data,
  };
};
/**
 *@param {error} error
 * @returns {object}
 */
export const getAppointmentsFailure = (error) => {
  return {
    type: types.GET_APPOINTMENTS_FAILURE,
    payload: error,
  };
};
/**
 *
 * @returns {object}
 */
export const addAppointmentsRequest = () => {
  return {
    type: types.POST_APPOINTMENTS_REQUEST,
  };
};
/**
 *
 * @param {object[]} data
 * @returns {object}
 */
export const addAppointmentsSuccess = (data) => {
  return {
    type: types.POST_APPOINTMENTS_SUCCESS,
    payload: data,
  };
};
/**
 *@param {error} error
 * @returns {object}
 */
export const addAppointmentsFailure = (error) => {
  return {
    type: types.POST_APPOINTMENTS_FAILURE,
    payload: error,
  };
};
