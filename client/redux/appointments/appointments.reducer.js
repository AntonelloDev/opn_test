import { produce } from 'immer';
import * as types from './appointments.types';

const appointmentReducer = (state, action) => {
  const { type, payload } = action;
  return produce(state, (draft) => {
    switch (type) {
      case types.GET_APPOINTMENTS_REQUEST:
        draft.getIsLoading = true;
        return draft;
      //
      case types.GET_APPOINTMENTS_SUCCESS:
        draft.data = payload;
        draft.getIsLoading = false;
        return draft;
      //
      case types.GET_APPOINTMENTS_FAILURE:
        draft.error = payload;
        draft.getIsLoading = false;
        return draft;
      //
      case types.POST_APPOINTMENTS_REQUEST:
        draft.postIsLoading = true;
        return draft;
      //
      case types.POST_APPOINTMENTS_SUCCESS:
        const personIndex = draft.data.findIndex(
          (ele) => ele?.name === payload.name
        );
        if (personIndex > -1 && draft.data[personIndex]?.appointments) {
          draft.data[personIndex].appointments.push(payload);
        } else {
          draft.data.push({
            name: payload.name,
            appointments: [payload],
          });
        }
        draft.postIsLoading = false;
        return draft;
      //
      case types.POST_APPOINTMENTS_FAILURE:
        draft.error = payload;
        draft.postIsLoading = false;
        return draft;
      //
      default:
        return state;
    }
  });
};
export default appointmentReducer;
