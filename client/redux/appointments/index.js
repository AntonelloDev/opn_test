import {
  getAppointmentsRequest,
  getAppointmentsSuccess,
  getAppointmentsFailure,
  addAppointmentsRequest,
  addAppointmentsSuccess,
  addAppointmentsFailure,
} from './appointments.actions';
import messages from '../../public/static/messages.json';

import API from '../../services/appointments';
// FETCH APPOINTMENTS:
export const fetchAppointments = () => (dispatch) => {
  dispatch(getAppointmentsRequest());
  API.fetchAppointments()
    .then((response) => {
      if (response && Array.isArray(response)) {
        dispatch(getAppointmentsSuccess(response));
      } else {
        dispatch(getAppointmentsFailure(messages.GENERIC_ERROR));
      }
    })
    .catch((error) => {
      dispatch(getAppointmentsFailure(error));
    });
};
// CREATE A NEW APPOINTMENT
export const createAppointment = (data) => (dispatch) => {
  dispatch(addAppointmentsRequest());
  API.createAppointments(data)
    .then((response) => {
      if (response && response.ok) {
        dispatch(addAppointmentsSuccess(data));
      } else {
        dispatch(addAppointmentsFailure(messages.GENERIC_ERROR));
      }
    })
    .catch((error) => {
      dispatch(addAppointmentsFailure(error));
    });
};
