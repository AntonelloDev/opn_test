export const people = [
  { label: 'Bill', id: 1 },
  { label: 'Mark', id: 2 },
  { label: 'Elon', id: 3 },
  { label: 'Tim', id: 4 },
];

export const spaces = [
  { label: 'living-room', color: '#17a2b8', id: 1 },
  { label: 'backyard', color: '#28a745', id: 2 },
  { label: 'kitchen', color: '#ffc107', id: 3 },
];
export const data = [
  {
    name: 'Bill',
    appointments: [
      { space: 'kitchen', start: '04:30', end: '05:00' },
      { space: 'backyard', start: '06:30', end: '07:30' },
      { space: 'living-room', start: '00:30', end: '02:00' },
    ],
  },
  {
    name: 'Mark',
    appointments: [{ space: 'living-room', start: '13:00', end: '14:00' }],
  },
  {
    name: 'Elon',
    appointments: [{ space: 'living-room', start: '11:30', end: '12:00' }],
  },

  {
    name: 'Tim',
    appointments: [
      { space: 'kitchen', start: '23:00', end: '00:00' },
      { space: 'kitchen', start: '10:30', end: '17:30' },
    ],
  },
];
