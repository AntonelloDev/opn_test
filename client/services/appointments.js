import axios from 'axios';
const headers = {
  'Content-Type': 'application/json',
};
const client = axios.create({
  baseURL: 'http://localhost:4000',
  timeout: 200000,
  headers,
});
const API = {
  /**
   *
   * @returns {object[]}
   */
  fetchAppointments: async () =>
    client
      .get('/')
      .then((res) => res?.data)
      .catch((error) => error),
  /**
   *
   * @param {object} data
   * @returns {object}
   */
  createAppointments: async (data) => {
    return client
      .post('/', data)
      .then((res) => res?.data)
      .catch((error) => error);
  },
};
export default API;
