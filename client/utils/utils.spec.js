import {
  colorSpaceMapper,
  joinClasses,
  createTimeHours,
  createTimeDropDownData,
  getLeftAndRightCellsTime,
  addHalfHour,
  generateTimeArray,
} from './utils';

describe('Join classes should take an array and return a string of elements separated by white space', () => {
  test("['one', 'two', 'three'] should return 'one two three'", () => {
    const expected = 'one two three';
    const actual = joinClasses(['one', 'two', 'three']);
    expect(actual).toEqual(expected);
  });
});
describe('colorSpaceMapper should return the correspondent color given a space name', () => {
  test('Expect living-room to return #17a2b8', () => {
    const expected = '#17a2b8';
    const actual = colorSpaceMapper('living-room');
    expect(actual).toEqual(expected);
  });
  test('Expect backyard to return #28a745', () => {
    const expected = '#28a745';
    const actual = colorSpaceMapper('backyard');
    expect(actual).toEqual(expected);
  });
  test('Expect kitchen to return #ffc107', () => {
    const expected = '#ffc107';
    const actual = colorSpaceMapper('kitchen');
    expect(actual).toEqual(expected);
  });
});
describe('createTimeHours should generate 24 hours, in the format 00:00 ... 23:00', () => {
  test("First element of the time array should be { time: '00:00', displayTime: '00:00' }", () => {
    const actual = createTimeHours();
    expect(actual[0]).toEqual({ time: '00:00', displayTime: '00:00' });
  });
  test("Third element of the time array should be { time: '00:30', displayTime: '00:00' }", () => {
    const actual = createTimeHours();
    expect(actual[2]).toEqual({ time: '01:00', displayTime: '01:00' });
  });
  test("Last element of the time array should be { time: '23:30', displayTime: '23:00' }", () => {
    const actual = createTimeHours();
    expect(actual[47]).toEqual({ time: '23:30', displayTime: '23:00' });
  });
});
describe('createTimeDropDownData should generate 24 hours, in the format {id:1, label:00:00} ... {id:2, label:23:00}', () => {
  test("First element of the time array should be { id: 0, label: '00:00' }", () => {
    const actual = createTimeDropDownData();
    expect(actual[0]).toEqual({ id: 0, label: '00:00' });
  });
  test("Last element of the time array should be { id: 23, label: '23:00' }", () => {
    const actual = createTimeDropDownData();
    expect(actual[1]).toEqual({ id: 1, label: '00:30' });
  });
  test("Last element of the time array should be { id: 23, label: '23:00' }", () => {
    const actual = createTimeDropDownData();
    expect(actual[2]).toEqual({ id: 2, label: '01:00' });
  });
  test("Mid element of the time array should be { id: 11, label: '11:00' }", () => {
    const actual = createTimeDropDownData();
    expect(actual[4]).toEqual({ id: 4, label: '02:00' });
  });
});
describe('getLeftAndRightCellsTime to take a string and split it in an object with left and right properties', () => {
  test("12:35 should return { left: '12', right: '35',}", () => {
    const expected = {
      left: '12',
      right: '35',
    };
    const actual = getLeftAndRightCellsTime('12:35');
    expect(actual).toEqual(expected);
  });
  test("23:00 should return { left: '23', right: '00',}", () => {
    const expected = {
      left: '23',
      right: '00',
    };
    const actual = getLeftAndRightCellsTime('23:00');
    expect(actual).toEqual(expected);
  });
});
describe('Should add 30 minutes to a given time in hh:mm format', () => {
  test('12:00 should return 12:30', () => {
    const expected = '12:30';
    const actual = addHalfHour('12:00');
    expect(actual).toEqual(expected);
  });
  test('11:30 should return 12:00', () => {
    const expected = '12:00';
    const actual = addHalfHour('11:30');
    expect(actual).toEqual(expected);
  });
});
describe('testing generateTimeArray', () => {
  test("'00:00', '02:00' should return ['00:00', '00:30', '01:00', '01:30']", () => {
    const expected = ['00:00', '00:30', '01:00', '01:30'];
    const actual = generateTimeArray('00:00', '02:00');
    expect(expected).toEqual(actual);
  });
  test("'01:00', '02:30' should return ['01:00', '01:30', '02:00']", () => {
    const expected = ['01:00', '01:30', '02:00'];
    const actual = generateTimeArray('01:00', '02:30');
    expect(expected).toEqual(actual);
  });
});
