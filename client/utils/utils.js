import moment from 'moment';
import colorSpacesMap from '../public/static/colorsSpaceMap.json';

/**
 *
 * @param {string[]} classes
 * @returns {string}
 */
export const joinClasses = (classes = []) => classes.join(' ');
/**
 *
 * @param {string} color
 * @returns {string}
 */
export const colorSpaceMapper = (color) => {
  if (color in colorSpacesMap) {
    return colorSpacesMap[color];
  }
  return '';
};
/**
 *
 * @param {string} time
 * @returns {string}
 */
export const addHalfHour = (time) => {
  const momentTime = moment(time, 'HH:mm', true)
    .add(30, 'minute')
    .format('HH:mm');
  return momentTime;
};

/**
 *
 * @returns {object[]}
 */
export const createTimeHours = () => {
  const times = [];
  let start = '00:00',
    end = '23:30';
  times.push({
    time: '00:00',
    displayTime: '00:00',
  });
  while (start < end) {
    start = addHalfHour(start);
    times.push({
      time: start,
      displayTime:
        start.split(':')[1] === '00' ? start : `${start.split(':')[0]}:00`,
    });
  }
  return times;
};
export const hours = createTimeHours();
/**
 *
 * @param {string[]} hours
 * @returns {object[]}
 */
export const createTimeDropDownData = () => {
  return [...hours, { time: '24:00', id: hours.length }].map((hour, index) => ({
    label: hour.time,
    id: index,
  }));
};
export const hoursDropDownData = createTimeDropDownData();
/**
 *
 * @param {string} time
 * @returns {object}
 */
export const getLeftAndRightCellsTime = (time) => {
  if (time)
    return {
      left: time.split(':')[0],
      right: time.split(':')[1],
    };
};

/**
 *
 * @param {string} time
 * @returns {moment.Moment }
 */
export const convertToMoment = (time) => moment(time, 'HH:mm', true);
/**
 *
 * @param {string} start
 * @param {string} end
 * @returns {string[]}
 */
export const generateTimeArray = (start, end) => {
  const timeArray = [];
  timeArray.push(convertToMoment(start).format('HH:mm'));

  while (start < end) {
    if (start >= '23:30') {
      break;
    }
    start = addHalfHour(start);
    if (start < end) {
      timeArray.push(convertToMoment(start).format('HH:mm'));
    }
  }
  return timeArray;
};
