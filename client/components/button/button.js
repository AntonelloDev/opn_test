import PropTypes from 'prop-types';
import styles from './button.module.css';

const Button = ({ text, onClick }) => {
  return (
    <button className={styles.button} onClick={onClick}>
      <span>
        <i className="fas fa-plus"></i>
      </span>
      <span>{text} </span>
    </button>
  );
};
Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
export default Button;
