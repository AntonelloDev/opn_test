import PropTypes from 'prop-types';
import styles from './header.module.css';

const Header = ({ title }) => {
  return (
    <div className={styles['header']}>
      <h1>{title}</h1>
      <hr />
    </div>
  );
};
Header.propTypes = {
  title: PropTypes.string,
};
Header.defaultProps = {
  title: 'House Timeline',
};
export default Header;
