import { render, screen } from '@testing-library/react';
import Header from './header';

describe('Header should be rendered correctly', () => {
  test('Header should be rendered with the given title', () => {
    render(<Header title="testing text" />);
    const title = screen.getByText('testing text');
    expect(title).toBeInTheDocument();
  });
});
