import Form from '../components/form/form';
import Calendar from '../components/calendar/calendar';
import Header from '../components/header/header';
import SpacesDisplay from '../components/spaces-display/SpacesDisplay';
import spaces from '../public/static/spaces.json';

export default function Index() {
  return (
    <div>
      <div>
        <Header />
        <Calendar />
        <SpacesDisplay spaces={spaces} />
        <Form title="Timeline entry" />
      </div>
    </div>
  );
}
