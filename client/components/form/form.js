import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Dropdown from '../dropdown/dropdown';
import Button from '../button/button';
import styles from './form.module.css';
import spaces from '../../public/static/spaces.json';
import people from '../../public/static/people.json';
import { hoursDropDownData, generateTimeArray } from '../../utils/utils';
import { useAlert } from 'react-alert';
import messages from '../../public/static/messages.json';
import { createAppointment } from '../../redux/appointments/index';

const DEFAULTS = {
  start: { label: '00:00', id: 1 },
  end: { label: '24:00', id: 1 },
  person: people[0],
  space: spaces[0],
};
/**
 *
 * @param {object[]} data
 * @param {object} newTime
 * @param {string} newTime.start
 * @param {string} newTime.end
 * @param {string} name
 * @returns {boolean}
 */
export const validateAppointment = (data, newTime, name) => {
  const person = data.find((element) => element.name === name);
  if (person) {
    const newAppointmentTimes = generateTimeArray(newTime.start, newTime.end);
    for (let i = 0; i < person.appointments.length; i++) {
      let { start, end } = person.appointments[i] ?? {};
      const appointmentTimes = generateTimeArray(start, end);
      for (let j = 0; j < newAppointmentTimes.length; j++) {
        if (appointmentTimes.includes(newAppointmentTimes[j])) {
          return false;
        }
      }
    }
  }
  return true;
};

const Form = ({ title }) => {
  const alert = useAlert();
  const dispatch = useDispatch();
  const { data, error } = useSelector((state) => state);
  const [fromTime, setFromTime] = useState(DEFAULTS.start);
  const [toTime, setToTime] = useState(DEFAULTS.end);
  const [person, setPerson] = useState(DEFAULTS.person);
  const [space, setSpace] = useState(DEFAULTS.space);

  /**
   *
   * @param {object} newTime
   */
  const handleFromTime = (newTime) => {
    setFromTime(newTime);
  };
  /**
   *
   * @param {object} newTime
   */
  const handleToTime = (newTime) => {
    setToTime(newTime);
  };
  const resetState = () => {
    setFromTime(DEFAULTS.start);
    setToTime(DEFAULTS.end);
    setPerson(DEFAULTS.person);
    setSpace(DEFAULTS.space);
  };
  const addToTimeLine = async (item) => {
    dispatch(createAppointment(item));
  };
  const handleSubmit = () => {
    const shallPass = validateAppointment(
      data,
      {
        start: fromTime.label,
        end: toTime.label,
      },
      person.label
    );
    if (shallPass) {
      addToTimeLine({
        start: fromTime.label,
        end: toTime.label,
        name: person.label,
        space: space.label,
      });
    } else {
      alert.error(messages.ITEM_DUPLICATE);
    }
    resetState();
  };
  useEffect(() => {
    if (error) {
      alert.error(error);
    }
  }, [error]);
  return (
    <div className={styles['form']}>
      <h3 className={styles['label']}>{title}</h3>
      <div className={styles['body']}>
        <div className={styles['inputs']}>
          <Dropdown
            setSelected={setPerson}
            label="name"
            selectedItem={person}
            data={people}
          />
          <Dropdown
            setSelected={setSpace}
            label="area"
            selectedItem={space}
            data={spaces}
          />
          <Dropdown
            label="from"
            data={hoursDropDownData}
            setSelected={handleFromTime}
            selectedItem={fromTime}
            maxTime={toTime?.label}
          />
          <Dropdown
            label="to"
            data={hoursDropDownData}
            setSelected={handleToTime}
            selectedItem={toTime}
            minTime={fromTime?.label}
          />
        </div>
        <Button onClick={handleSubmit} text="Add" />
      </div>
    </div>
  );
};

Form.propTypes = {
  title: PropTypes.string,
};
export default Form;
