import { validateAppointment } from './form';
import { data } from '../../mockData';

describe('testing validateAppointment, it should not allow to over book slots', () => {
  test('01:30 should not be allowed for Bill', () => {
    const expected = false;
    const actual = validateAppointment(
      data,
      { start: '01:30', end: '02:00' },
      'Bill'
    );
    expect(expected).toEqual(actual);
  });
  test('02:00 should be allowed for Bill', () => {
    const expected = true;
    const actual = validateAppointment(
      data,
      { start: '02:00', end: '04:00' },
      'Bill'
    );
    expect(expected).toEqual(actual);
  });
  test('00:00, 01:30 should not be allowed for Bill', () => {
    const expected = false;
    const actual = validateAppointment(
      data,
      { start: '00:00', end: '01:30' },
      'Bill'
    );
    expect(expected).toEqual(actual);
  });
  test('00:00, 00:30 should be allowed for Bill', () => {
    const expected = true;
    const actual = validateAppointment(
      data,
      { start: '00:00', end: '00:30' },
      'Bill'
    );
    expect(expected).toEqual(actual);
  });
  test('11:30 should not be allowed for Elon', () => {
    const expected = false;
    const actual = validateAppointment(data, { start: '11:30' }, 'Elon');
    expect(expected).toEqual(actual);
  });
  test('02:00 should be allowed for Elon', () => {
    const expected = true;
    const actual = validateAppointment(data, { start: '02:00' }, 'Elon');
    expect(expected).toEqual(actual);
  });
  test('11:30 should not be allowed for Elon', () => {
    const expected = false;
    const actual = validateAppointment(data, { start: '11:30' }, 'Elon');
    expect(expected).toEqual(actual);
  });
  test('15:00 should not be allowed for Tim', () => {
    const expected = false;
    const actual = validateAppointment(
      data,
      { start: '15:00', end: '15:30' },
      'Tim'
    );
    expect(expected).toEqual(actual);
  });
  test('17:30 should be allowed for Tim', () => {
    const expected = true;
    const actual = validateAppointment(
      data,
      { start: '17:30', end: '18:00' },
      'Tim'
    );
    expect(expected).toEqual(actual);
  });
});
