import styles from './SpacesDisplay.module.css';
import PropTypes from 'prop-types';

const SpacesDisplay = ({ spaces }) => {
  return (
    <ul className={styles['container']}>
      {spaces.map((space) => (
        <li key={space.color}>
          <span
            className={styles['coloured-cell']}
            style={{ backgroundColor: space.color }}
          />
          <span>{space.label}</span>
        </li>
      ))}
    </ul>
  );
};
SpacesDisplay.propTypes = {
  spaces: PropTypes.array.isRequired,
};
export default SpacesDisplay;
