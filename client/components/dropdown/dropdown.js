import PropTypes from 'prop-types';
import { useState } from 'react';
import styles from './dropdown.module.css';

const Dropdown = ({
  label,
  data,
  selectedItem,
  setSelected = () => {},
  minTime,
  maxTime,
}) => {
  const [isOpen, setOpen] = useState(false);
  const toggleDropdown = () => setOpen(!isOpen);
  /**
   *
   * @param {object} item
   */
  const handleClick = (item) => {
    setSelected(item);
    toggleDropdown();
  };
  return (
    <div className={styles['container']}>
      <p className={styles['label']}>{label}</p>
      <div className={styles['dropdown']}>
        <div className={styles['header']} onClick={toggleDropdown}>
          {selectedItem?.label}
          <i
            className={`fa fa-chevron-right ${styles['icon']} ${
              isOpen && styles['open']
            }`}
          ></i>
        </div>
        <div className={`${styles['body']} ${isOpen && styles['open']}`}>
          {data.map(
            (item) =>
              (item.label > minTime || !minTime) &&
              (item.label < maxTime || !maxTime) && (
                <div
                  key={item.id}
                  className={styles['item']}
                  onClick={(e) => handleClick(item)}
                  id={item.id}
                >
                  <span
                    className={`${styles['item-dot']} ${
                      item.id == selectedItem?.id && styles['selected']
                    }`}
                  >
                    •{' '}
                  </span>
                  {item.label}
                </div>
              )
          )}
        </div>
      </div>
    </div>
  );
};
Dropdown.propTypes = {
  label: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  selectedItem: PropTypes.object,
  setSelected: PropTypes.func.isRequired,
  minTime: PropTypes.string,
  maxTime: PropTypes.string,
};
export default Dropdown;
