import styles from './cell.module.css';
import { joinClasses } from '../../utils/utils';

const Cell = (props) => {
  return props.left && props.right ? (
    <>
      <div className={styles['cell']}>{props.left}</div>
      <span>:</span>
      <div className={styles['cell']}>{props.right}</div>
    </>
  ) : (
    <>
      <div
        style={{ background: props.color }}
        className={joinClasses([styles['cell'], styles['border']])}
      />
    </>
  );
};
export default Cell;
