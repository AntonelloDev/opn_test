import { getColouredTimes } from './calendar';

describe('Testing getColouredTimes function, it should generate an array of times to be coloured', () => {
  test("It should return the following list of times: '00:30', '01:00', '01:30', '04:30', '06:30', '07:00'", () => {
    const mockAppointments = [
      { space: 'kitchen', start: '04:30', end: '05:00' },
      { space: 'backyard', start: '06:30', end: '07:30' },
      { space: 'living-room', start: '00:30', end: '02:00' },
    ];
    const expected = [
      { space: 'kitchen', times: ['04:30'] },
      { space: 'backyard', times: ['06:30', '07:00'] },
      { space: 'living-room', times: ['00:30', '01:00', '01:30'] },
    ];
    const actual = getColouredTimes(mockAppointments);
    expect(expected).toEqual(actual);
  });
  test("It should return the following list of times: 13:00', '13:30'", () => {
    const mockAppointments = [
      { space: 'living-room', start: '13:00', end: '14:00' },
    ];
    const expected = [
      {
        space: 'living-room',
        times: ['13:00', '13:30'],
      },
    ];
    const actual = getColouredTimes(mockAppointments);
    expect(expected).toEqual(actual);
  });
});
