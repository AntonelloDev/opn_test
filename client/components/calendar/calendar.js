import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { generateTimeArray } from '../../utils/utils';
import styles from './calendar.module.css';
import Cell from './cell';
import Skeleton from 'react-loading-skeleton';
import { fetchAppointments } from '../../redux/appointments/index';
import names from '../../public/static/people.json';

import {
  hours,
  getLeftAndRightCellsTime,
  colorSpaceMapper,
} from '../../utils/utils';
/**
 *
 * @param {object[]} appointments
 * @param {string} space
 */
export const getColouredTimes = (appointments) => {
  const times = [];
  appointments.forEach((appointment) => {
    let { start, end, space } = appointment;
    const timeSheet = {
      times: [],
      space,
    };
    timeSheet.times.push(...generateTimeArray(start, end));
    times.push(timeSheet);
  });
  return times.sort();
};
export const RowTime = () => (
  <div className={styles['row-container']}>
    <div className={styles['time-row']}>
      <div className={styles['cells']}>
        {hours.map(
          (cell, i) =>
            i % 2 == 0 && (
              <span key={cell.time}>
                <div className={styles['cell']}>
                  <Cell {...getLeftAndRightCellsTime(cell.displayTime)} />
                </div>{' '}
              </span>
            )
        )}
      </div>
    </div>
  </div>
);
export const RowData = ({ data = [] }) => {
  return names.map((name, i) => {
    const element = data.find((ele) => ele.name === name.label);
    if (element) {
      return (
        <div className={styles['row-container']} key={element?.name ?? i}>
          <div className={styles['row']}>
            <span className={styles['label']}>{element?.name}</span>
            <div className={[styles['cells']]}>
              {hours.map((cell) => {
                let color = 'unset';
                const times = getColouredTimes(element?.appointments);
                for (let i = 0; i < times.length; i++) {
                  if (times[i]?.times.includes(cell.time)) {
                    color = colorSpaceMapper(times[i]?.space);
                  }
                }
                return <Cell key={cell.time} {...element} color={color} />;
              })}
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className={styles['row-container']} key={name.id}>
        <div className={styles['row']}>
          <span className={styles['label']}>{name.label}</span>
          <div className={[styles['cells']]}>
            {hours.map((cell) => {
              return <Cell key={cell.time} />;
            })}
          </div>
        </div>
      </div>
    );
  });
};

const Calendar = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAppointments());
  }, []);
  const { postIsLoading, getIsLoading, data } = useSelector((state) => state);
  return (
    <div className={styles['container']}>
      <h3>Timeline</h3>
      {postIsLoading || getIsLoading ? (
        <div style={{ fontSize: 50 }}>
          <Skeleton count={4} />
        </div>
      ) : (
        <>
          <RowTime />
          <RowData data={data} />
        </>
      )}
    </div>
  );
};
export default Calendar;
